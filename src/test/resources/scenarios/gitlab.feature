@GitlabFeatures
  Feature: Gitlab Frontend
    Scenario: Admin access denied
      Given non-admin user is logged in
      When user accesses admin ui
      Then page is not found
    Scenario: Sign up should complete
      Given user is not logged in
      And random credentials
      When user visits global home page
      And account creation is initiated
      And signup is performed with provided credentials
      Then there should be a flash notice saying "You have signed up successfully. However, we could not sign you in because your account is awaiting approval from your GitLab administrator."
    Scenario: Login with invalid password fails
      Given user is not logged in
      And invalid password is used
      When user visits global home page
      And login is performed with provided credentials
      Then there should be an error saying "Invalid login or password."
    Scenario: Search should hide others
      Given user is logged in
      When preferences page is visited
      Then navigation block is visible
      When search is performed with query "Localization"
      And after waiting for 1000
      Then navigation theme block should NOT be visible
    Scenario: Search should show the result
      Given user is logged in
      When preferences page is visited
      Then navigation block is visible
      When search is performed with query "Navigation"
      And after waiting for 1000
      Then navigation theme block should be visible
    Scenario: Timezone should be correct
      Given user is logged in
      When profile page is visited
      Then timezone should be "[UTC 0] UTC"
    Scenario: Email should match the one used upon the signup
      Given user is logged in
      When profile page is visited
      Then email should be "definitely_not_this_one@nikitavbv.com"
    Scenario: Edit file should complete and should result in file chaging
      Given user is logged in
      And test project is opened
      And readme is opened
      And file size is saved for comparison
      When file is edited
      And table is added
      And changes are committed
      Then file size should not be the same