package com.nikitavbv.testing.gitlab.pages;

import static com.nikitavbv.testing.gitlab.Utils.path;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AdminPage {

  private final WebDriver driver;

  private static final By notFoundBy = By.xpath("/html/body/div/div/h3");

  public AdminPage(WebDriver driver) {
    this.driver = driver;
  }

  public AdminPage navigate() {
    driver.get(path("/admin"));
    return this;
  }

  public boolean isNotFound() {
    return driver.findElement(notFoundBy).getText().contains("Page Not Found");
  }
}
