package com.nikitavbv.testing.gitlab.pages;

import static com.nikitavbv.testing.gitlab.Utils.path;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PreferencesPage {

  private final WebDriver driver;

  private static final By navigationBlockBy = By.id("navigation-theme");
  private static final By searchBy = By.xpath("/html/body/div[3]/div/div[3]/main/div[2]/div/input");

  public PreferencesPage(WebDriver driver) {
    this.driver = driver;
  }

  public void navigate() {
    driver.get(path("/-/profile/preferences"));
  }

  public PreferencesPage performSearch(String query) {
    driver.findElement(searchBy).sendKeys(query);
    return this;
  }

  public boolean isNavigationThemeBlockVisible() {
    return !driver.findElement(navigationBlockBy).findElement(By.xpath("./..")).getCssValue("display").equals("none");
  }
}
