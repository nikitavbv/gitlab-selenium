package com.nikitavbv.testing.gitlab.pages;

import static com.nikitavbv.testing.gitlab.Utils.path;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProfilePage {

  private final WebDriver driver;

  private static final By timeZoneBy = By.xpath("/html/body/div[3]/div/div[3]/main/form/div[3]/div[2]/div/button");
  private static final By emailBy = By.id("user_email");

  public ProfilePage(WebDriver driver) {
    this.driver = driver;
  }

  public ProfilePage navigate() {
    driver.get(path("/-/profile"));
    return this;
  }

  public String getTimeZone() {
    return driver.findElement(timeZoneBy).getText();
  }

  public String getEmail() {
    return driver.findElement(emailBy).getText();
  }
}
