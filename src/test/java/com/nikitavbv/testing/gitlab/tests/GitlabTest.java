package com.nikitavbv.testing.gitlab.tests;

import static com.nikitavbv.testing.gitlab.Utils.TEST_PROJECT;
import static com.nikitavbv.testing.gitlab.Utils.TEST_USERNAME;
import static com.nikitavbv.testing.gitlab.Utils.testPassword;
import static com.nikitavbv.testing.gitlab.Utils.testUsername;

import com.nikitavbv.testing.gitlab.pages.AdminPage;
import com.nikitavbv.testing.gitlab.pages.HomePage;
import com.nikitavbv.testing.gitlab.pages.PreferencesPage;
import com.nikitavbv.testing.gitlab.pages.ProfilePage;
import com.nikitavbv.testing.gitlab.pages.ProjectPage;
import com.nikitavbv.testing.gitlab.pages.UserHomePage;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.junit5.SerenityTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

@SerenityTest
public class GitlabTest {

  protected WebDriver driver;

  @BeforeEach
  public void setup() {
    if (System.getenv().containsKey("CI_SERVER")) {
      System.out.println("connecting to webdriver container");
      try {
        driver = new RemoteWebDriver(new URL("http://selenium__standalone-chrome:4444/wd/hub"), new ChromeOptions().setHeadless(true));
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
      } catch (MalformedURLException e) {
        e.printStackTrace();
      }
    } else {
      System.out.println("connecting to local webdriver");
      driver = new ChromeDriver();
    }
  }

  @AfterEach
  public void close() {
    driver.quit();
  }

  protected HomePage globalHomePage() {
    return new HomePage(driver).navigate();
  }

  protected UserHomePage authorized() {
    if (this.driver == null) {
      setup();
    }

    var homePage = globalHomePage();
    return homePage.login(testUsername(), testPassword());
  }

  protected ProjectPage testProject() {
    var home = authorized();
    var projectPage = new ProjectPage(driver);
    projectPage.navigate(TEST_USERNAME, TEST_PROJECT);
    return projectPage;
  }

  protected PreferencesPage preferencesPage() {
    var home = authorized();
    var preferencesPage = new PreferencesPage(driver);
    preferencesPage.navigate();
    return preferencesPage;
  }

  protected AdminPage adminPage() {
    var adminPage = new AdminPage(driver);
    adminPage.navigate();
    return adminPage;
  }

  protected ProfilePage profilePage() {
    var home = authorized();
    var profilePage = new ProfilePage(driver);
    profilePage.navigate();
    return profilePage;
  }
}
