package com.nikitavbv.testing.gitlab;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features={"src/test/resources/scenarios"}, tags="@GitlabFeatures", glue="com.nikitavbv.testing.gitlab.steps")
public class CucumberTests {
}
