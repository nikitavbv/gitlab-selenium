package com.nikitavbv.testing.gitlab.steps;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.nikitavbv.testing.gitlab.pages.ProfilePage;
import com.nikitavbv.testing.gitlab.tests.GitlabTest;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ProfilePageSteps extends GitlabTest {

  private ProfilePage profilePage;

  @When("profile page is visited")
  public void profilePageIsVisited() {
    profilePage = profilePage();
  }

  @Then("timezone should be {string}")
  public void timeZoneShouldBe(String tz) {
    assertEquals(tz, profilePage.getTimeZone());
  }

  @Then("email should be {string}")
  public void emailShouldBe(String expectedEmail) {
    assertEquals(expectedEmail, profilePage.getEmail());
  }
}
