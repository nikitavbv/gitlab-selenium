package com.nikitavbv.testing.gitlab.steps;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import com.nikitavbv.testing.gitlab.pages.EditorPage;
import com.nikitavbv.testing.gitlab.pages.ProjectFilePage;
import com.nikitavbv.testing.gitlab.pages.ProjectPage;
import com.nikitavbv.testing.gitlab.tests.GitlabTest;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class RepositorySteps extends GitlabTest {

  private ProjectPage project;
  private ProjectFilePage filePage;
  private EditorPage editorPage;

  private String fileSize;

  @Given("test project is opened")
  public void testProjectIsOpened() {
    project = testProject();
  }

  @Given("readme is opened")
  public void readmeIsOpened() {
    filePage = project.openReadmeFile();
  }

  @Given("file size is saved for comparison")
  public void fileSizeIsSavedForComparison() {
    fileSize = filePage.fileSize();
  }

  @When("file is edited")
  public void fileIsEdited() {
    editorPage = filePage.editFile();
  }

  @When("table is added")
  public void tableIsAdded() {
    editorPage.addTable();
  }

  @When("changes are committed")
  public void changesAreCommitted() {
    filePage = editorPage.commitChanges();
  }

  @Then("file size should not be the same")
  public void fileSizeShouldNotBeTheSame() {
    assertNotEquals(fileSize, filePage.fileSize());
  }
}
