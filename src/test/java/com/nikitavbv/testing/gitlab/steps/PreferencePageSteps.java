package com.nikitavbv.testing.gitlab.steps;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.nikitavbv.testing.gitlab.pages.PreferencesPage;
import com.nikitavbv.testing.gitlab.tests.GitlabTest;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PreferencePageSteps extends GitlabTest {

  private PreferencesPage preferencesPage;

  @When("preferences page is visited")
  public void preferencesPageIsVisited() {
    preferencesPage = preferencesPage();
  }

  @When("search is performed with query {string}")
  public void searchPerformedWithQuery(String query) {
    preferencesPage.performSearch(query);
  }

  @When("after waiting for {int}")
  public void afterWaitingFor(int millis) throws InterruptedException {
    Thread.sleep(millis);
  }

  @Then("navigation block is visible")
  public void navigationBlockIsVisible() {
    assertTrue(preferencesPage.isNavigationThemeBlockVisible());
  }

  @Then("navigation theme block should NOT be visible")
  public void navigationBlockShouldNotBeVisible() {
    assertFalse(preferencesPage.isNavigationThemeBlockVisible());
  }

  @Then("navigation theme block should be visible")
  public void navigationBlockShouldBeVisible() {
    assertTrue(preferencesPage.isNavigationThemeBlockVisible());
  }
}
