package com.nikitavbv.testing.gitlab.steps;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.nikitavbv.testing.gitlab.pages.AdminPage;
import com.nikitavbv.testing.gitlab.tests.GitlabTest;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AdminPageSteps extends GitlabTest {

  private AdminPage adminPage;

  @When("non-admin user is logged in")
  public void login_non_admin() {
    authorized();
  }

  @When("user accesses admin ui")
  public void user_accesses_admin_ui() {
    this.adminPage = adminPage();
  }

  @Then("page is not found")
  public void page_is_not_found() {
    assertTrue(adminPage.isNotFound());
  }
}
