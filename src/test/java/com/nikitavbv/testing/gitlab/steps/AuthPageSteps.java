package com.nikitavbv.testing.gitlab.steps;

import static com.nikitavbv.testing.gitlab.Utils.TEST_USERNAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.nikitavbv.testing.gitlab.pages.HomePage;
import com.nikitavbv.testing.gitlab.pages.SignUpPage;
import com.nikitavbv.testing.gitlab.tests.GitlabTest;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang3.RandomStringUtils;

public class AuthPageSteps extends GitlabTest {

  private String firstName;
  private String lastName;
  private String username;
  private String email;
  private String password;

  private HomePage homePage;
  private SignUpPage signUpPage;

  @Given("user is not logged in")
  public void userNotLoggedIn() {
    setup();
  }

  @Given("random credentials")
  public void randomCredentials() {
    firstName = RandomStringUtils.randomAlphabetic(10);
    lastName = RandomStringUtils.randomAlphabetic(10);
    username = RandomStringUtils.randomAlphabetic(10);
    email = RandomStringUtils.randomAlphabetic(10) + "@example.com";
    password = RandomStringUtils.randomAlphanumeric(10);
  }

  @Given("invalid password is used")
  public void invalidPasswordIsUsed() {
    username = TEST_USERNAME;
    password = "invalid_password";
  }

  @When("user visits global home page")
  public void userVisitsGlobalHomePage() {
    homePage = globalHomePage();
  }

  @When("account creation is initiated")
  public void accountCreationIsInitiated() {
    signUpPage = homePage.initiateAccountCreation();
  }

  @When("login is performed with provided credentials")
  public void loginIsPerformedWithProvidedCredentials() {
    homePage.login(username, password);
  }

  @When("user is logged in")
  public void userIsLoggedIn() {
    authorized();
  }

  @When("signup is performed with provided credentials")
  public void signupIsPerformedWithProvidedCredentials() {
    homePage = signUpPage.register(firstName, lastName, username, email, password);
  }

  @Then("there should be a flash notice saying {string}")
  public void thereShouldBeFlashNoticeSaying(String text) {
    assertTrue(homePage.flashNoticeText().contains(text));
  }

  @Then("there should be an error saying {string}")
  public void thereShouldBeErrorSaying(String text) {
    assertEquals(text, homePage.errorMessageText());
  }
}
